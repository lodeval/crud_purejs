const plats2ul = (plats) => {
    const ul = document.createElement('ul')
    ul.id = "foodlist"
    ul.innerHTML = plats.map(plat2li).join('')
    return ul
}

const plat2li = (plat) => `
    <li class="plat" data-weight="${plat.weight}" data-calories="${plat.caloriesTotal}">
        <p>
        ${plat.name} 
            <a data-action="delete" href="/food/${plat.id}" >
                delete
            </a>
            <button id="edit" data-action="submit" href="/food/${plat.id}" >
                edit
            </button>
        </p>
    </li>
`
module.exports = (emit, mountPoint) => {
const Component = require('../component')(state)
return new Component(
    mountPoint,
    function (state){
        this.mountPoint.hidden = !state.view.includes(this)
        this.mountPoint.innerHTML = plats2ul(state.plats).innerHTML
    },
    comp => {
    comp.mountPoint.addEventListener('click', (e) => {
        e.preventDefault()
        if (e.target.matches('a')){
            const targetUrl = e.target.attributes.href.value
            emit('delete resource', targetUrl)
        } if (e.target.matches('p')){
            const targetUrl = e.target.firstElementChild.getAttribute('href')
            emit('show', targetUrl)
        } if (e.target.matches('#edit')){
            const targetUrl = e.target.attributes.href.value
            emit('edit button clicked', targetUrl)
        }
    })
    }
)
}