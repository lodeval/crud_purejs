const detailsPlat = (plat) =>`
<label>Nom du plat
    <input type="text" name="name" value="${plat.name}"/>
</label>
<label>Description
    <input type="text" name="description" value="${plat.description}"/>
</label>
<label>Poids
    <input type="text" name="weight" value="${plat.weight}"/>
</label>
<label>Proteines
    <input type="text" name="proteins" value="${plat.protines}"/>
</label>
<label>Glucide
    <input type="text" name="carbohydrates" value="${plat.carbohydrates}"/>
</label>
<label>Lipides
    <input type="text" name="lipids" value="${plat.lipids}"/>
</label>
    <input type="hidden" name="id" value=${plat.id}/> 
<input type="submit" name="submit" value="Enregistrer" />
`

module.exports = (emit, mountPoint) => {
    const Component = require('../component')(state)
    return new Component(
        mountPoint,
        function(state){
        this.mountPoint.hidden = !state.view.includes(this)
        this.mountPoint.innerHTML = detailsPlat(state.element)
        this.mountPoint.querySelector('input').focus()
        },
        comp =>
        comp.mountPoint.addEventListener('submit', (e) => {
            e.preventDefault()
            let editedResource = Object.fromEntries(new FormData(comp.mountPoint))
            state.plate=editedResource
            console.log(`ressource éditée:`, state.plate)
            console.log(state.view)
            editUrl=state.view
            emit('edit element', editUrl)
        })
    )
}
