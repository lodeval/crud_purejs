const Food = require('./models/food')
const backFood2food = (bf) => ({
    name: bf.name,
    lipides: bf.lipids,
    glucides: bf.carbohydrates,
    proteines: bf.proteins,
    weight: bf.weight
})

const food2backFood = (f) => ({
    name: f.name,
    lipids: f.lipides,
    carbohydrates: f.glucides,
    proteins: f.proteines,
    weitht: f.weight
})


module.exports = (backend) => {
    let state = new Proxy(
    {
        observers: [],
        views: {'/': []},
        view: '/',
        actions: {},
        addObserver: function(o){this.observers.push(o)}
    },
    {
        set: function(target, prop, val){
        const oldval = target[prop]
        target[prop] = val
        switch (prop) {
        case 'views':
        case 'actions':
        case 'observers':
            break;
        case 'view':
            // hide old components
            target.views[oldval].forEach(c => c.mountPoint.hidden = true)
            console.log(`view changed to ${val}, will display :`)
            console.log(target.views[val])
            target.views[val].forEach(o => { // render then show
            o.render(state)
            o.mountPoint.hidden = false
            })
            history.replaceState(null,'', val)
        }
        return val
        },
        get: function(target, prop){
            switch (prop) {
            case 'target':
            return target
            case 'backend':
            return target.backend
            case 'actions':
            return target.actions
            default:
            return target[prop]
            }
        }
        }
    )

state.target.backend = backend

state.actions.loadFood = function(){
    return fetch(`${state.backend}/food`)
    .then(res => res.json())
    .then(list => list.map(p => Object.assign(new Food(backFood2food(p)), {id: p.id})))
    .then(plats => state.plats = plats)
}

state.actions.createFood = (food) => {
    console.log(`food creation with ${food}`)
    return fetch(`${state.backend}/food`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(food)
    })
}

state.actions.deleteFood = (targetUrl) =>
    fetch(`${state.backend}${targetUrl}`,
    {
        method: 'delete'
    }
    )
    .then(state.actions.loadFood)

state.actions.editFood = (editUrl) => {
    console.log(`food edition page : ${state.backend}${editUrl}`)
    console.log(`ressource éditée:`, state.plate)
    return fetch(`${state.backend}${editUrl}`,
    {
        method: 'PUT',
        headers: {
            'Access-Control-Allow-Headers':'*',
            'Access-Control-Allow-Origin':'*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(state.plate)
    })
}

return state

}