(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
// state
const State = require('./src/state')
const backend = 'http://localhost:2999'
let state = State(backend)
window.state = state

// controller
const controller = require('pubsub')
window.controller = controller

//compose UI: list of top elements
const listMountPoint = document.createElement('ul')
const counterMountPoint = document.createElement('div')
const addFoodMountPoint = document.createElement('button')
const createFormMountPoint = document.createElement('form')
const detailsMountPoint = document.createElement('div')
const editMountPoint = document.createElement('form')


// components
const listComp = require('./src/components/list')(controller.publish,listMountPoint)
const counterComp = require('./src/components/counter')(controller.publish, counterMountPoint)
const createComp = require('./src/components/addbutton')(controller.publish, addFoodMountPoint)
const createForm = require('./src/components/create_form')(controller.publish, createFormMountPoint)
const detailsComp = require('./src/components/detailed_list')(controller.publish, detailsMountPoint)
const editComp = require ('./src/components/edit') (controller.publish, editMountPoint)



const components = [ listComp, counterComp, createComp, createForm, detailsComp, editComp]
components.forEach(comp => state.addObserver(comp))
components.map(c => c.mountPoint).forEach(mp => document.body.appendChild(mp))

// views
state.views = {
    '/' : [listComp, counterComp, createComp],
    '/food': [listComp, counterComp, createComp],
    '/food/new': [createForm],
    'food/show': [detailsComp],
    '/food/edit': [editComp]
}

// controller configuration
controller.subscribe('addfood button clicked', () => {
    state.view = '/food/new'
    history.pushState("{}", "",state.view)
})
controller.subscribe(
    'init',
    () =>
    state.actions.loadFood()
    .then(() => state.view = '/food'),
    history.pushState("{}", "",'/food')
)
controller.subscribe(
    'create new ressource',
    (newFood) =>
    state.actions.createFood(newFood)
    .then(state.actions.loadFood)
    .then(() => state.view = '/food')
)
controller.subscribe(
    'update ressource',
    (newFood) =>
    state.actions.updatePlat(newFood)
        .then(state.actions.loadFood)
        .then(() => state.view = '/food')
)  
controller.subscribe(
    'edit resource',
    (url) => {
        state.actions.editFood(url)
        history.pushState("{}", "",'/food/edit')
        .then(() => state.view = '/food/edit')
    }
)

controller.subscribe(
    'edit plate', (url) => {
        state.actions.editFood(url)
        state.view='/food/edit',
        history.pushState("{}", "", state.view)
    }
)

controller.subscribe(
    'delete resource',
    (url) =>
    state.actions.deleteFood(url)
    .then(state.actions.loadFood)
    .then(() => state.view = '/food')
    .then(() => state.view = '/food')
)

controller.subscribe(
    'show',
    (url) => {
        let id = parseInt(url.replace('/food/',''))
        const obj = state.plats.find(o => o.id === id)
        state.element = obj
        state.views[url] = [detailsComp]
        state.view = url
    }
)

controller.subscribe(
    'edit button clicked',
    (url) => {
        let id = parseInt(url.replace('/food/',''))
        const obj = state.plats.find(o => o.id === id)
        state.element = obj
        state.views[url] = [editComp]
        state.view = url
    }
)

controller.subscribe(
    'edit element',
    (plat) =>
        state.actions.editFood(plat)
        .then(state.actions.loadFood)
        .then(() => state.view = '/food')
)

// application initialization

controller.publish('init') // initial action, fetch data, decide first view

window.addEventListener('popstate', (e) => {
    console.log('pathname = ${location.pathname}')
    state.view = location.pathname
})  
},{"./src/components/addbutton":4,"./src/components/counter":5,"./src/components/create_form":6,"./src/components/detailed_list":7,"./src/components/edit":8,"./src/components/list":9,"./src/state":11,"pubsub":2}],2:[function(require,module,exports){
// subscriber store
let topics = {}

// example:
// topics = {topic1: [subscriber1, subscriber2], topic2: [subscriber3]}

let subscribe = (topic, subscriber) => {
  // initializes topic if not existent
  topics[topic] = topics[topic] || []
  // push subscriber to topic's listener
  topics[topic].push(subscriber)
}

let publish = (topic, option) => {
  // for each registered subscriber to given topic, call subscriber(option)
  if (!topics[topic])
    return // topic not defined
  console.log(`bus received ${topic} with args=${option}`)
  topics[topic].forEach(subscriber => subscriber(option))
}

module.exports = { subscribe: subscribe, publish: publish }

},{}],3:[function(require,module,exports){
module.exports = (state) => function Component(mountPoint, render, initEvents) {
    this.mountPoint = mountPoint
    this.render = render
    this.state = state
    if (initEvents && typeof initEvents === "function")
        initEvents(this)
}
},{}],4:[function(require,module,exports){
// emit = controller.publish
// reference to state is replaced by emit

module.exports = (emit, mountPoint) => {
    const Component = require('../component')(state)
    return new Component(
        mountPoint,
        function(state){
        this.mountPoint.type = 'button'
        this.mountPoint.innerText = 'Créer un aliment'
        this.mountPoint.hidden = !state.view.includes(this)
        },
    comp =>
        comp.mountPoint.addEventListener('click', () => {
          // comp.state.view = comp.state.views.addFood
        emit('addfood button clicked')
        })
    )
}
},{"../component":3}],5:[function(require,module,exports){
module.exports = (state, mountPoint) => {
    const Component = require('../component')(state)
    return new Component(
    mountPoint,
    function(state){
        this.mountPoint.innerHTML = `${state.plats.length} aliments`
        this.mountPoint.hidden = !state.view.includes(this)
    }
    )
}
},{"../component":3}],6:[function(require,module,exports){
const template = `
    <label>Nom
        <input type="text" name="name" size="20"/>
    </label>
    <label>Description
        <input type="text" name="description" size="20" />
    </label>
    <label>Lipides
        <input type="text" name="lipids" size="5" />
    </label>
    <input type="submit" name="submit" value="Créer" />
`

module.exports = (emit, mountPoint) => {
    const Component = require('../component')(state)
    return new Component(
        mountPoint,
        function(state){
        this.mountPoint.hidden = !state.view.includes(this)
        this.mountPoint.innerHTML = template
        this.mountPoint.querySelector('input').focus()
    },
    comp =>
        comp.mountPoint.addEventListener('submit', (e) => {
        e.preventDefault()
        let newResource = Object.fromEntries(new FormData(comp.mountPoint))
        console.log(newResource)
        emit('create new ressource', newResource)
        })
        )
}
},{"../component":3}],7:[function(require,module,exports){
const platDetails = (plat) =>
`
<p>Nom du plat : ${plat.name}</p>
<p>Poids : ${plat.weight}</p>
<p>Calories : ${plat.calories}</p>
<p>Protéines : ${plat.proteines}</p>
<p>Glucides : ${plat.glucides}</p>
<p>Lipides : ${plat.lipides}</p>
`

module.exports = (state, mountPoint) => {
    const Component = require('../component')(state)
    return new Component(
        mountPoint,
        function (state){
            this.mountPoint.hidden = !state.view.includes(this)
            this.mountPoint.innerHTML = platDetails(state.element)
        }
    )
}

},{"../component":3}],8:[function(require,module,exports){
const detailsPlat = (plat) =>`
<label>Nom du plat
    <input type="text" name="name" value="${plat.name}"/>
</label>
<label>Description
    <input type="text" name="description" value="${plat.description}"/>
</label>
<label>Poids
    <input type="text" name="weight" value="${plat.weight}"/>
</label>
<label>Proteines
    <input type="text" name="proteins" value="${plat.protines}"/>
</label>
<label>Glucide
    <input type="text" name="carbohydrates" value="${plat.carbohydrates}"/>
</label>
<label>Lipides
    <input type="text" name="lipids" value="${plat.lipids}"/>
</label>
    <input type="hidden" name="id" value=${plat.id}/> 
<input type="submit" name="submit" value="Enregistrer" />
`

module.exports = (emit, mountPoint) => {
    const Component = require('../component')(state)
    return new Component(
        mountPoint,
        function(state){
        this.mountPoint.hidden = !state.view.includes(this)
        this.mountPoint.innerHTML = detailsPlat(state.element)
        this.mountPoint.querySelector('input').focus()
        },
        comp =>
        comp.mountPoint.addEventListener('submit', (e) => {
            e.preventDefault()
            let editedResource = Object.fromEntries(new FormData(comp.mountPoint))
            state.plate=editedResource
            console.log(`ressource éditée:`, state.plate)
            console.log(state.view)
            editUrl=state.view
            emit('edit element', editUrl)
        })
    )
}

},{"../component":3}],9:[function(require,module,exports){
const plats2ul = (plats) => {
    const ul = document.createElement('ul')
    ul.id = "foodlist"
    ul.innerHTML = plats.map(plat2li).join('')
    return ul
}

const plat2li = (plat) => `
    <li class="plat" data-weight="${plat.weight}" data-calories="${plat.caloriesTotal}">
        <p>
        ${plat.name} 
            <a data-action="delete" href="/food/${plat.id}" >
                delete
            </a>
            <button id="edit" data-action="submit" href="/food/${plat.id}" >
                edit
            </button>
        </p>
    </li>
`
module.exports = (emit, mountPoint) => {
const Component = require('../component')(state)
return new Component(
    mountPoint,
    function (state){
        this.mountPoint.hidden = !state.view.includes(this)
        this.mountPoint.innerHTML = plats2ul(state.plats).innerHTML
    },
    comp => {
    comp.mountPoint.addEventListener('click', (e) => {
        e.preventDefault()
        if (e.target.matches('a')){
            const targetUrl = e.target.attributes.href.value
            emit('delete resource', targetUrl)
        } if (e.target.matches('p')){
            const targetUrl = e.target.firstElementChild.getAttribute('href')
            emit('show', targetUrl)
        } if (e.target.matches('#edit')){
            const targetUrl = e.target.attributes.href.value
            emit('edit button clicked', targetUrl)
        }
    })
    }
)
}
},{"../component":3}],10:[function(require,module,exports){
class Food{
    constructor(nameOrObject, weight, caloriesTotal, lipides, glucides, proteines){
        if (nameOrObject instanceof Object) {
            Object.assign(this, nameOrObject)
        } else {
            this.name = nameOrObject
            this.weight = weight
            this.lipides = lipides
            this.glucides = glucides
            this.proteines = proteines
        }
        }
        get calories () { return this.lipides*9+this.glucides*4+this.proteines*4}
    }
    
Object.defineProperty(Food.prototype,'caloriesTotal', {
    get: function(){
    return  this.calories * this.weight / 100
    }
})



Food.message = 'this is in food module'

module.exports = Food 
},{}],11:[function(require,module,exports){
const Food = require('./models/food')
const backFood2food = (bf) => ({
    name: bf.name,
    lipides: bf.lipids,
    glucides: bf.carbohydrates,
    proteines: bf.proteins,
    weight: bf.weight
})

const food2backFood = (f) => ({
    name: f.name,
    lipids: f.lipides,
    carbohydrates: f.glucides,
    proteins: f.proteines,
    weitht: f.weight
})


module.exports = (backend) => {
    let state = new Proxy(
    {
        observers: [],
        views: {'/': []},
        view: '/',
        actions: {},
        addObserver: function(o){this.observers.push(o)}
    },
    {
        set: function(target, prop, val){
        const oldval = target[prop]
        target[prop] = val
        switch (prop) {
        case 'views':
        case 'actions':
        case 'observers':
            break;
        case 'view':
            // hide old components
            target.views[oldval].forEach(c => c.mountPoint.hidden = true)
            console.log(`view changed to ${val}, will display :`)
            console.log(target.views[val])
            target.views[val].forEach(o => { // render then show
            o.render(state)
            o.mountPoint.hidden = false
            })
            history.replaceState(null,'', val)
        }
        return val
        },
        get: function(target, prop){
            switch (prop) {
            case 'target':
            return target
            case 'backend':
            return target.backend
            case 'actions':
            return target.actions
            default:
            return target[prop]
            }
        }
        }
    )

state.target.backend = backend

state.actions.loadFood = function(){
    return fetch(`${state.backend}/food`)
    .then(res => res.json())
    .then(list => list.map(p => Object.assign(new Food(backFood2food(p)), {id: p.id})))
    .then(plats => state.plats = plats)
}

state.actions.createFood = (food) => {
    console.log(`food creation with ${food}`)
    return fetch(`${state.backend}/food`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(food)
    })
}

state.actions.deleteFood = (targetUrl) =>
    fetch(`${state.backend}${targetUrl}`,
    {
        method: 'delete'
    }
    )
    .then(state.actions.loadFood)

state.actions.editFood = (editUrl) => {
    console.log(`food edition page : ${state.backend}${editUrl}`)
    console.log(`ressource éditée:`, state.plate)
    return fetch(`${state.backend}${editUrl}`,
    {
        method: 'PUT',
        headers: {
            'Access-Control-Allow-Headers':'*',
            'Access-Control-Allow-Origin':'*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(state.plate)
    })
}

return state

}
},{"./models/food":10}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJtYWluLmpzIiwibm9kZV9tb2R1bGVzL3B1YnN1Yi9pbmRleC5qcyIsInNyYy9jb21wb25lbnQuanMiLCJzcmMvY29tcG9uZW50cy9hZGRidXR0b24uanMiLCJzcmMvY29tcG9uZW50cy9jb3VudGVyLmpzIiwic3JjL2NvbXBvbmVudHMvY3JlYXRlX2Zvcm0uanMiLCJzcmMvY29tcG9uZW50cy9kZXRhaWxlZF9saXN0LmpzIiwic3JjL2NvbXBvbmVudHMvZWRpdC5qcyIsInNyYy9jb21wb25lbnRzL2xpc3QuanMiLCJzcmMvbW9kZWxzL2Zvb2QuanMiLCJzcmMvc3RhdGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbklBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ05BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNUQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDNUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM1Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIi8vIHN0YXRlXG5jb25zdCBTdGF0ZSA9IHJlcXVpcmUoJy4vc3JjL3N0YXRlJylcbmNvbnN0IGJhY2tlbmQgPSAnaHR0cDovL2xvY2FsaG9zdDoyOTk5J1xubGV0IHN0YXRlID0gU3RhdGUoYmFja2VuZClcbndpbmRvdy5zdGF0ZSA9IHN0YXRlXG5cbi8vIGNvbnRyb2xsZXJcbmNvbnN0IGNvbnRyb2xsZXIgPSByZXF1aXJlKCdwdWJzdWInKVxud2luZG93LmNvbnRyb2xsZXIgPSBjb250cm9sbGVyXG5cbi8vY29tcG9zZSBVSTogbGlzdCBvZiB0b3AgZWxlbWVudHNcbmNvbnN0IGxpc3RNb3VudFBvaW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgndWwnKVxuY29uc3QgY291bnRlck1vdW50UG9pbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuY29uc3QgYWRkRm9vZE1vdW50UG9pbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdidXR0b24nKVxuY29uc3QgY3JlYXRlRm9ybU1vdW50UG9pbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdmb3JtJylcbmNvbnN0IGRldGFpbHNNb3VudFBvaW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbmNvbnN0IGVkaXRNb3VudFBvaW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZm9ybScpXG5cblxuLy8gY29tcG9uZW50c1xuY29uc3QgbGlzdENvbXAgPSByZXF1aXJlKCcuL3NyYy9jb21wb25lbnRzL2xpc3QnKShjb250cm9sbGVyLnB1Ymxpc2gsbGlzdE1vdW50UG9pbnQpXG5jb25zdCBjb3VudGVyQ29tcCA9IHJlcXVpcmUoJy4vc3JjL2NvbXBvbmVudHMvY291bnRlcicpKGNvbnRyb2xsZXIucHVibGlzaCwgY291bnRlck1vdW50UG9pbnQpXG5jb25zdCBjcmVhdGVDb21wID0gcmVxdWlyZSgnLi9zcmMvY29tcG9uZW50cy9hZGRidXR0b24nKShjb250cm9sbGVyLnB1Ymxpc2gsIGFkZEZvb2RNb3VudFBvaW50KVxuY29uc3QgY3JlYXRlRm9ybSA9IHJlcXVpcmUoJy4vc3JjL2NvbXBvbmVudHMvY3JlYXRlX2Zvcm0nKShjb250cm9sbGVyLnB1Ymxpc2gsIGNyZWF0ZUZvcm1Nb3VudFBvaW50KVxuY29uc3QgZGV0YWlsc0NvbXAgPSByZXF1aXJlKCcuL3NyYy9jb21wb25lbnRzL2RldGFpbGVkX2xpc3QnKShjb250cm9sbGVyLnB1Ymxpc2gsIGRldGFpbHNNb3VudFBvaW50KVxuY29uc3QgZWRpdENvbXAgPSByZXF1aXJlICgnLi9zcmMvY29tcG9uZW50cy9lZGl0JykgKGNvbnRyb2xsZXIucHVibGlzaCwgZWRpdE1vdW50UG9pbnQpXG5cblxuXG5jb25zdCBjb21wb25lbnRzID0gWyBsaXN0Q29tcCwgY291bnRlckNvbXAsIGNyZWF0ZUNvbXAsIGNyZWF0ZUZvcm0sIGRldGFpbHNDb21wLCBlZGl0Q29tcF1cbmNvbXBvbmVudHMuZm9yRWFjaChjb21wID0+IHN0YXRlLmFkZE9ic2VydmVyKGNvbXApKVxuY29tcG9uZW50cy5tYXAoYyA9PiBjLm1vdW50UG9pbnQpLmZvckVhY2gobXAgPT4gZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChtcCkpXG5cbi8vIHZpZXdzXG5zdGF0ZS52aWV3cyA9IHtcbiAgICAnLycgOiBbbGlzdENvbXAsIGNvdW50ZXJDb21wLCBjcmVhdGVDb21wXSxcbiAgICAnL2Zvb2QnOiBbbGlzdENvbXAsIGNvdW50ZXJDb21wLCBjcmVhdGVDb21wXSxcbiAgICAnL2Zvb2QvbmV3JzogW2NyZWF0ZUZvcm1dLFxuICAgICdmb29kL3Nob3cnOiBbZGV0YWlsc0NvbXBdLFxuICAgICcvZm9vZC9lZGl0JzogW2VkaXRDb21wXVxufVxuXG4vLyBjb250cm9sbGVyIGNvbmZpZ3VyYXRpb25cbmNvbnRyb2xsZXIuc3Vic2NyaWJlKCdhZGRmb29kIGJ1dHRvbiBjbGlja2VkJywgKCkgPT4ge1xuICAgIHN0YXRlLnZpZXcgPSAnL2Zvb2QvbmV3J1xuICAgIGhpc3RvcnkucHVzaFN0YXRlKFwie31cIiwgXCJcIixzdGF0ZS52aWV3KVxufSlcbmNvbnRyb2xsZXIuc3Vic2NyaWJlKFxuICAgICdpbml0JyxcbiAgICAoKSA9PlxuICAgIHN0YXRlLmFjdGlvbnMubG9hZEZvb2QoKVxuICAgIC50aGVuKCgpID0+IHN0YXRlLnZpZXcgPSAnL2Zvb2QnKSxcbiAgICBoaXN0b3J5LnB1c2hTdGF0ZShcInt9XCIsIFwiXCIsJy9mb29kJylcbilcbmNvbnRyb2xsZXIuc3Vic2NyaWJlKFxuICAgICdjcmVhdGUgbmV3IHJlc3NvdXJjZScsXG4gICAgKG5ld0Zvb2QpID0+XG4gICAgc3RhdGUuYWN0aW9ucy5jcmVhdGVGb29kKG5ld0Zvb2QpXG4gICAgLnRoZW4oc3RhdGUuYWN0aW9ucy5sb2FkRm9vZClcbiAgICAudGhlbigoKSA9PiBzdGF0ZS52aWV3ID0gJy9mb29kJylcbilcbmNvbnRyb2xsZXIuc3Vic2NyaWJlKFxuICAgICd1cGRhdGUgcmVzc291cmNlJyxcbiAgICAobmV3Rm9vZCkgPT5cbiAgICBzdGF0ZS5hY3Rpb25zLnVwZGF0ZVBsYXQobmV3Rm9vZClcbiAgICAgICAgLnRoZW4oc3RhdGUuYWN0aW9ucy5sb2FkRm9vZClcbiAgICAgICAgLnRoZW4oKCkgPT4gc3RhdGUudmlldyA9ICcvZm9vZCcpXG4pICBcbmNvbnRyb2xsZXIuc3Vic2NyaWJlKFxuICAgICdlZGl0IHJlc291cmNlJyxcbiAgICAodXJsKSA9PiB7XG4gICAgICAgIHN0YXRlLmFjdGlvbnMuZWRpdEZvb2QodXJsKVxuICAgICAgICBoaXN0b3J5LnB1c2hTdGF0ZShcInt9XCIsIFwiXCIsJy9mb29kL2VkaXQnKVxuICAgICAgICAudGhlbigoKSA9PiBzdGF0ZS52aWV3ID0gJy9mb29kL2VkaXQnKVxuICAgIH1cbilcblxuY29udHJvbGxlci5zdWJzY3JpYmUoXG4gICAgJ2VkaXQgcGxhdGUnLCAodXJsKSA9PiB7XG4gICAgICAgIHN0YXRlLmFjdGlvbnMuZWRpdEZvb2QodXJsKVxuICAgICAgICBzdGF0ZS52aWV3PScvZm9vZC9lZGl0JyxcbiAgICAgICAgaGlzdG9yeS5wdXNoU3RhdGUoXCJ7fVwiLCBcIlwiLCBzdGF0ZS52aWV3KVxuICAgIH1cbilcblxuY29udHJvbGxlci5zdWJzY3JpYmUoXG4gICAgJ2RlbGV0ZSByZXNvdXJjZScsXG4gICAgKHVybCkgPT5cbiAgICBzdGF0ZS5hY3Rpb25zLmRlbGV0ZUZvb2QodXJsKVxuICAgIC50aGVuKHN0YXRlLmFjdGlvbnMubG9hZEZvb2QpXG4gICAgLnRoZW4oKCkgPT4gc3RhdGUudmlldyA9ICcvZm9vZCcpXG4gICAgLnRoZW4oKCkgPT4gc3RhdGUudmlldyA9ICcvZm9vZCcpXG4pXG5cbmNvbnRyb2xsZXIuc3Vic2NyaWJlKFxuICAgICdzaG93JyxcbiAgICAodXJsKSA9PiB7XG4gICAgICAgIGxldCBpZCA9IHBhcnNlSW50KHVybC5yZXBsYWNlKCcvZm9vZC8nLCcnKSlcbiAgICAgICAgY29uc3Qgb2JqID0gc3RhdGUucGxhdHMuZmluZChvID0+IG8uaWQgPT09IGlkKVxuICAgICAgICBzdGF0ZS5lbGVtZW50ID0gb2JqXG4gICAgICAgIHN0YXRlLnZpZXdzW3VybF0gPSBbZGV0YWlsc0NvbXBdXG4gICAgICAgIHN0YXRlLnZpZXcgPSB1cmxcbiAgICB9XG4pXG5cbmNvbnRyb2xsZXIuc3Vic2NyaWJlKFxuICAgICdlZGl0IGJ1dHRvbiBjbGlja2VkJyxcbiAgICAodXJsKSA9PiB7XG4gICAgICAgIGxldCBpZCA9IHBhcnNlSW50KHVybC5yZXBsYWNlKCcvZm9vZC8nLCcnKSlcbiAgICAgICAgY29uc3Qgb2JqID0gc3RhdGUucGxhdHMuZmluZChvID0+IG8uaWQgPT09IGlkKVxuICAgICAgICBzdGF0ZS5lbGVtZW50ID0gb2JqXG4gICAgICAgIHN0YXRlLnZpZXdzW3VybF0gPSBbZWRpdENvbXBdXG4gICAgICAgIHN0YXRlLnZpZXcgPSB1cmxcbiAgICB9XG4pXG5cbmNvbnRyb2xsZXIuc3Vic2NyaWJlKFxuICAgICdlZGl0IGVsZW1lbnQnLFxuICAgIChwbGF0KSA9PlxuICAgICAgICBzdGF0ZS5hY3Rpb25zLmVkaXRGb29kKHBsYXQpXG4gICAgICAgIC50aGVuKHN0YXRlLmFjdGlvbnMubG9hZEZvb2QpXG4gICAgICAgIC50aGVuKCgpID0+IHN0YXRlLnZpZXcgPSAnL2Zvb2QnKVxuKVxuXG4vLyBhcHBsaWNhdGlvbiBpbml0aWFsaXphdGlvblxuXG5jb250cm9sbGVyLnB1Ymxpc2goJ2luaXQnKSAvLyBpbml0aWFsIGFjdGlvbiwgZmV0Y2ggZGF0YSwgZGVjaWRlIGZpcnN0IHZpZXdcblxud2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3BvcHN0YXRlJywgKGUpID0+IHtcbiAgICBjb25zb2xlLmxvZygncGF0aG5hbWUgPSAke2xvY2F0aW9uLnBhdGhuYW1lfScpXG4gICAgc3RhdGUudmlldyA9IGxvY2F0aW9uLnBhdGhuYW1lXG59KSAgIiwiLy8gc3Vic2NyaWJlciBzdG9yZVxubGV0IHRvcGljcyA9IHt9XG5cbi8vIGV4YW1wbGU6XG4vLyB0b3BpY3MgPSB7dG9waWMxOiBbc3Vic2NyaWJlcjEsIHN1YnNjcmliZXIyXSwgdG9waWMyOiBbc3Vic2NyaWJlcjNdfVxuXG5sZXQgc3Vic2NyaWJlID0gKHRvcGljLCBzdWJzY3JpYmVyKSA9PiB7XG4gIC8vIGluaXRpYWxpemVzIHRvcGljIGlmIG5vdCBleGlzdGVudFxuICB0b3BpY3NbdG9waWNdID0gdG9waWNzW3RvcGljXSB8fCBbXVxuICAvLyBwdXNoIHN1YnNjcmliZXIgdG8gdG9waWMncyBsaXN0ZW5lclxuICB0b3BpY3NbdG9waWNdLnB1c2goc3Vic2NyaWJlcilcbn1cblxubGV0IHB1Ymxpc2ggPSAodG9waWMsIG9wdGlvbikgPT4ge1xuICAvLyBmb3IgZWFjaCByZWdpc3RlcmVkIHN1YnNjcmliZXIgdG8gZ2l2ZW4gdG9waWMsIGNhbGwgc3Vic2NyaWJlcihvcHRpb24pXG4gIGlmICghdG9waWNzW3RvcGljXSlcbiAgICByZXR1cm4gLy8gdG9waWMgbm90IGRlZmluZWRcbiAgY29uc29sZS5sb2coYGJ1cyByZWNlaXZlZCAke3RvcGljfSB3aXRoIGFyZ3M9JHtvcHRpb259YClcbiAgdG9waWNzW3RvcGljXS5mb3JFYWNoKHN1YnNjcmliZXIgPT4gc3Vic2NyaWJlcihvcHRpb24pKVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHsgc3Vic2NyaWJlOiBzdWJzY3JpYmUsIHB1Ymxpc2g6IHB1Ymxpc2ggfVxuIiwibW9kdWxlLmV4cG9ydHMgPSAoc3RhdGUpID0+IGZ1bmN0aW9uIENvbXBvbmVudChtb3VudFBvaW50LCByZW5kZXIsIGluaXRFdmVudHMpIHtcbiAgICB0aGlzLm1vdW50UG9pbnQgPSBtb3VudFBvaW50XG4gICAgdGhpcy5yZW5kZXIgPSByZW5kZXJcbiAgICB0aGlzLnN0YXRlID0gc3RhdGVcbiAgICBpZiAoaW5pdEV2ZW50cyAmJiB0eXBlb2YgaW5pdEV2ZW50cyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBpbml0RXZlbnRzKHRoaXMpXG59IiwiLy8gZW1pdCA9IGNvbnRyb2xsZXIucHVibGlzaFxuLy8gcmVmZXJlbmNlIHRvIHN0YXRlIGlzIHJlcGxhY2VkIGJ5IGVtaXRcblxubW9kdWxlLmV4cG9ydHMgPSAoZW1pdCwgbW91bnRQb2ludCkgPT4ge1xuICAgIGNvbnN0IENvbXBvbmVudCA9IHJlcXVpcmUoJy4uL2NvbXBvbmVudCcpKHN0YXRlKVxuICAgIHJldHVybiBuZXcgQ29tcG9uZW50KFxuICAgICAgICBtb3VudFBvaW50LFxuICAgICAgICBmdW5jdGlvbihzdGF0ZSl7XG4gICAgICAgIHRoaXMubW91bnRQb2ludC50eXBlID0gJ2J1dHRvbidcbiAgICAgICAgdGhpcy5tb3VudFBvaW50LmlubmVyVGV4dCA9ICdDcsOpZXIgdW4gYWxpbWVudCdcbiAgICAgICAgdGhpcy5tb3VudFBvaW50LmhpZGRlbiA9ICFzdGF0ZS52aWV3LmluY2x1ZGVzKHRoaXMpXG4gICAgICAgIH0sXG4gICAgY29tcCA9PlxuICAgICAgICBjb21wLm1vdW50UG9pbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XG4gICAgICAgICAgLy8gY29tcC5zdGF0ZS52aWV3ID0gY29tcC5zdGF0ZS52aWV3cy5hZGRGb29kXG4gICAgICAgIGVtaXQoJ2FkZGZvb2QgYnV0dG9uIGNsaWNrZWQnKVxuICAgICAgICB9KVxuICAgIClcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IChzdGF0ZSwgbW91bnRQb2ludCkgPT4ge1xuICAgIGNvbnN0IENvbXBvbmVudCA9IHJlcXVpcmUoJy4uL2NvbXBvbmVudCcpKHN0YXRlKVxuICAgIHJldHVybiBuZXcgQ29tcG9uZW50KFxuICAgIG1vdW50UG9pbnQsXG4gICAgZnVuY3Rpb24oc3RhdGUpe1xuICAgICAgICB0aGlzLm1vdW50UG9pbnQuaW5uZXJIVE1MID0gYCR7c3RhdGUucGxhdHMubGVuZ3RofSBhbGltZW50c2BcbiAgICAgICAgdGhpcy5tb3VudFBvaW50LmhpZGRlbiA9ICFzdGF0ZS52aWV3LmluY2x1ZGVzKHRoaXMpXG4gICAgfVxuICAgIClcbn0iLCJjb25zdCB0ZW1wbGF0ZSA9IGBcbiAgICA8bGFiZWw+Tm9tXG4gICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIG5hbWU9XCJuYW1lXCIgc2l6ZT1cIjIwXCIvPlxuICAgIDwvbGFiZWw+XG4gICAgPGxhYmVsPkRlc2NyaXB0aW9uXG4gICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIG5hbWU9XCJkZXNjcmlwdGlvblwiIHNpemU9XCIyMFwiIC8+XG4gICAgPC9sYWJlbD5cbiAgICA8bGFiZWw+TGlwaWRlc1xuICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBuYW1lPVwibGlwaWRzXCIgc2l6ZT1cIjVcIiAvPlxuICAgIDwvbGFiZWw+XG4gICAgPGlucHV0IHR5cGU9XCJzdWJtaXRcIiBuYW1lPVwic3VibWl0XCIgdmFsdWU9XCJDcsOpZXJcIiAvPlxuYFxuXG5tb2R1bGUuZXhwb3J0cyA9IChlbWl0LCBtb3VudFBvaW50KSA9PiB7XG4gICAgY29uc3QgQ29tcG9uZW50ID0gcmVxdWlyZSgnLi4vY29tcG9uZW50Jykoc3RhdGUpXG4gICAgcmV0dXJuIG5ldyBDb21wb25lbnQoXG4gICAgICAgIG1vdW50UG9pbnQsXG4gICAgICAgIGZ1bmN0aW9uKHN0YXRlKXtcbiAgICAgICAgdGhpcy5tb3VudFBvaW50LmhpZGRlbiA9ICFzdGF0ZS52aWV3LmluY2x1ZGVzKHRoaXMpXG4gICAgICAgIHRoaXMubW91bnRQb2ludC5pbm5lckhUTUwgPSB0ZW1wbGF0ZVxuICAgICAgICB0aGlzLm1vdW50UG9pbnQucXVlcnlTZWxlY3RvcignaW5wdXQnKS5mb2N1cygpXG4gICAgfSxcbiAgICBjb21wID0+XG4gICAgICAgIGNvbXAubW91bnRQb2ludC5hZGRFdmVudExpc3RlbmVyKCdzdWJtaXQnLCAoZSkgPT4ge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgbGV0IG5ld1Jlc291cmNlID0gT2JqZWN0LmZyb21FbnRyaWVzKG5ldyBGb3JtRGF0YShjb21wLm1vdW50UG9pbnQpKVxuICAgICAgICBjb25zb2xlLmxvZyhuZXdSZXNvdXJjZSlcbiAgICAgICAgZW1pdCgnY3JlYXRlIG5ldyByZXNzb3VyY2UnLCBuZXdSZXNvdXJjZSlcbiAgICAgICAgfSlcbiAgICAgICAgKVxufSIsImNvbnN0IHBsYXREZXRhaWxzID0gKHBsYXQpID0+XG5gXG48cD5Ob20gZHUgcGxhdCA6ICR7cGxhdC5uYW1lfTwvcD5cbjxwPlBvaWRzIDogJHtwbGF0LndlaWdodH08L3A+XG48cD5DYWxvcmllcyA6ICR7cGxhdC5jYWxvcmllc308L3A+XG48cD5Qcm90w6lpbmVzIDogJHtwbGF0LnByb3RlaW5lc308L3A+XG48cD5HbHVjaWRlcyA6ICR7cGxhdC5nbHVjaWRlc308L3A+XG48cD5MaXBpZGVzIDogJHtwbGF0LmxpcGlkZXN9PC9wPlxuYFxuXG5tb2R1bGUuZXhwb3J0cyA9IChzdGF0ZSwgbW91bnRQb2ludCkgPT4ge1xuICAgIGNvbnN0IENvbXBvbmVudCA9IHJlcXVpcmUoJy4uL2NvbXBvbmVudCcpKHN0YXRlKVxuICAgIHJldHVybiBuZXcgQ29tcG9uZW50KFxuICAgICAgICBtb3VudFBvaW50LFxuICAgICAgICBmdW5jdGlvbiAoc3RhdGUpe1xuICAgICAgICAgICAgdGhpcy5tb3VudFBvaW50LmhpZGRlbiA9ICFzdGF0ZS52aWV3LmluY2x1ZGVzKHRoaXMpXG4gICAgICAgICAgICB0aGlzLm1vdW50UG9pbnQuaW5uZXJIVE1MID0gcGxhdERldGFpbHMoc3RhdGUuZWxlbWVudClcbiAgICAgICAgfVxuICAgIClcbn1cbiIsImNvbnN0IGRldGFpbHNQbGF0ID0gKHBsYXQpID0+YFxuPGxhYmVsPk5vbSBkdSBwbGF0XG4gICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgbmFtZT1cIm5hbWVcIiB2YWx1ZT1cIiR7cGxhdC5uYW1lfVwiLz5cbjwvbGFiZWw+XG48bGFiZWw+RGVzY3JpcHRpb25cbiAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBuYW1lPVwiZGVzY3JpcHRpb25cIiB2YWx1ZT1cIiR7cGxhdC5kZXNjcmlwdGlvbn1cIi8+XG48L2xhYmVsPlxuPGxhYmVsPlBvaWRzXG4gICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgbmFtZT1cIndlaWdodFwiIHZhbHVlPVwiJHtwbGF0LndlaWdodH1cIi8+XG48L2xhYmVsPlxuPGxhYmVsPlByb3RlaW5lc1xuICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIG5hbWU9XCJwcm90ZWluc1wiIHZhbHVlPVwiJHtwbGF0LnByb3RpbmVzfVwiLz5cbjwvbGFiZWw+XG48bGFiZWw+R2x1Y2lkZVxuICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIG5hbWU9XCJjYXJib2h5ZHJhdGVzXCIgdmFsdWU9XCIke3BsYXQuY2FyYm9oeWRyYXRlc31cIi8+XG48L2xhYmVsPlxuPGxhYmVsPkxpcGlkZXNcbiAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBuYW1lPVwibGlwaWRzXCIgdmFsdWU9XCIke3BsYXQubGlwaWRzfVwiLz5cbjwvbGFiZWw+XG4gICAgPGlucHV0IHR5cGU9XCJoaWRkZW5cIiBuYW1lPVwiaWRcIiB2YWx1ZT0ke3BsYXQuaWR9Lz4gXG48aW5wdXQgdHlwZT1cInN1Ym1pdFwiIG5hbWU9XCJzdWJtaXRcIiB2YWx1ZT1cIkVucmVnaXN0cmVyXCIgLz5cbmBcblxubW9kdWxlLmV4cG9ydHMgPSAoZW1pdCwgbW91bnRQb2ludCkgPT4ge1xuICAgIGNvbnN0IENvbXBvbmVudCA9IHJlcXVpcmUoJy4uL2NvbXBvbmVudCcpKHN0YXRlKVxuICAgIHJldHVybiBuZXcgQ29tcG9uZW50KFxuICAgICAgICBtb3VudFBvaW50LFxuICAgICAgICBmdW5jdGlvbihzdGF0ZSl7XG4gICAgICAgIHRoaXMubW91bnRQb2ludC5oaWRkZW4gPSAhc3RhdGUudmlldy5pbmNsdWRlcyh0aGlzKVxuICAgICAgICB0aGlzLm1vdW50UG9pbnQuaW5uZXJIVE1MID0gZGV0YWlsc1BsYXQoc3RhdGUuZWxlbWVudClcbiAgICAgICAgdGhpcy5tb3VudFBvaW50LnF1ZXJ5U2VsZWN0b3IoJ2lucHV0JykuZm9jdXMoKVxuICAgICAgICB9LFxuICAgICAgICBjb21wID0+XG4gICAgICAgIGNvbXAubW91bnRQb2ludC5hZGRFdmVudExpc3RlbmVyKCdzdWJtaXQnLCAoZSkgPT4ge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICBsZXQgZWRpdGVkUmVzb3VyY2UgPSBPYmplY3QuZnJvbUVudHJpZXMobmV3IEZvcm1EYXRhKGNvbXAubW91bnRQb2ludCkpXG4gICAgICAgICAgICBzdGF0ZS5wbGF0ZT1lZGl0ZWRSZXNvdXJjZVxuICAgICAgICAgICAgY29uc29sZS5sb2coYHJlc3NvdXJjZSDDqWRpdMOpZTpgLCBzdGF0ZS5wbGF0ZSlcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHN0YXRlLnZpZXcpXG4gICAgICAgICAgICBlZGl0VXJsPXN0YXRlLnZpZXdcbiAgICAgICAgICAgIGVtaXQoJ2VkaXQgZWxlbWVudCcsIGVkaXRVcmwpXG4gICAgICAgIH0pXG4gICAgKVxufVxuIiwiY29uc3QgcGxhdHMydWwgPSAocGxhdHMpID0+IHtcbiAgICBjb25zdCB1bCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3VsJylcbiAgICB1bC5pZCA9IFwiZm9vZGxpc3RcIlxuICAgIHVsLmlubmVySFRNTCA9IHBsYXRzLm1hcChwbGF0MmxpKS5qb2luKCcnKVxuICAgIHJldHVybiB1bFxufVxuXG5jb25zdCBwbGF0MmxpID0gKHBsYXQpID0+IGBcbiAgICA8bGkgY2xhc3M9XCJwbGF0XCIgZGF0YS13ZWlnaHQ9XCIke3BsYXQud2VpZ2h0fVwiIGRhdGEtY2Fsb3JpZXM9XCIke3BsYXQuY2Fsb3JpZXNUb3RhbH1cIj5cbiAgICAgICAgPHA+XG4gICAgICAgICR7cGxhdC5uYW1lfSBcbiAgICAgICAgICAgIDxhIGRhdGEtYWN0aW9uPVwiZGVsZXRlXCIgaHJlZj1cIi9mb29kLyR7cGxhdC5pZH1cIiA+XG4gICAgICAgICAgICAgICAgZGVsZXRlXG4gICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICA8YnV0dG9uIGlkPVwiZWRpdFwiIGRhdGEtYWN0aW9uPVwic3VibWl0XCIgaHJlZj1cIi9mb29kLyR7cGxhdC5pZH1cIiA+XG4gICAgICAgICAgICAgICAgZWRpdFxuICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgIDwvcD5cbiAgICA8L2xpPlxuYFxubW9kdWxlLmV4cG9ydHMgPSAoZW1pdCwgbW91bnRQb2ludCkgPT4ge1xuY29uc3QgQ29tcG9uZW50ID0gcmVxdWlyZSgnLi4vY29tcG9uZW50Jykoc3RhdGUpXG5yZXR1cm4gbmV3IENvbXBvbmVudChcbiAgICBtb3VudFBvaW50LFxuICAgIGZ1bmN0aW9uIChzdGF0ZSl7XG4gICAgICAgIHRoaXMubW91bnRQb2ludC5oaWRkZW4gPSAhc3RhdGUudmlldy5pbmNsdWRlcyh0aGlzKVxuICAgICAgICB0aGlzLm1vdW50UG9pbnQuaW5uZXJIVE1MID0gcGxhdHMydWwoc3RhdGUucGxhdHMpLmlubmVySFRNTFxuICAgIH0sXG4gICAgY29tcCA9PiB7XG4gICAgY29tcC5tb3VudFBvaW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGUpID0+IHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgIGlmIChlLnRhcmdldC5tYXRjaGVzKCdhJykpe1xuICAgICAgICAgICAgY29uc3QgdGFyZ2V0VXJsID0gZS50YXJnZXQuYXR0cmlidXRlcy5ocmVmLnZhbHVlXG4gICAgICAgICAgICBlbWl0KCdkZWxldGUgcmVzb3VyY2UnLCB0YXJnZXRVcmwpXG4gICAgICAgIH0gaWYgKGUudGFyZ2V0Lm1hdGNoZXMoJ3AnKSl7XG4gICAgICAgICAgICBjb25zdCB0YXJnZXRVcmwgPSBlLnRhcmdldC5maXJzdEVsZW1lbnRDaGlsZC5nZXRBdHRyaWJ1dGUoJ2hyZWYnKVxuICAgICAgICAgICAgZW1pdCgnc2hvdycsIHRhcmdldFVybClcbiAgICAgICAgfSBpZiAoZS50YXJnZXQubWF0Y2hlcygnI2VkaXQnKSl7XG4gICAgICAgICAgICBjb25zdCB0YXJnZXRVcmwgPSBlLnRhcmdldC5hdHRyaWJ1dGVzLmhyZWYudmFsdWVcbiAgICAgICAgICAgIGVtaXQoJ2VkaXQgYnV0dG9uIGNsaWNrZWQnLCB0YXJnZXRVcmwpXG4gICAgICAgIH1cbiAgICB9KVxuICAgIH1cbilcbn0iLCJjbGFzcyBGb29ke1xuICAgIGNvbnN0cnVjdG9yKG5hbWVPck9iamVjdCwgd2VpZ2h0LCBjYWxvcmllc1RvdGFsLCBsaXBpZGVzLCBnbHVjaWRlcywgcHJvdGVpbmVzKXtcbiAgICAgICAgaWYgKG5hbWVPck9iamVjdCBpbnN0YW5jZW9mIE9iamVjdCkge1xuICAgICAgICAgICAgT2JqZWN0LmFzc2lnbih0aGlzLCBuYW1lT3JPYmplY3QpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLm5hbWUgPSBuYW1lT3JPYmplY3RcbiAgICAgICAgICAgIHRoaXMud2VpZ2h0ID0gd2VpZ2h0XG4gICAgICAgICAgICB0aGlzLmxpcGlkZXMgPSBsaXBpZGVzXG4gICAgICAgICAgICB0aGlzLmdsdWNpZGVzID0gZ2x1Y2lkZXNcbiAgICAgICAgICAgIHRoaXMucHJvdGVpbmVzID0gcHJvdGVpbmVzXG4gICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBnZXQgY2Fsb3JpZXMgKCkgeyByZXR1cm4gdGhpcy5saXBpZGVzKjkrdGhpcy5nbHVjaWRlcyo0K3RoaXMucHJvdGVpbmVzKjR9XG4gICAgfVxuICAgIFxuT2JqZWN0LmRlZmluZVByb3BlcnR5KEZvb2QucHJvdG90eXBlLCdjYWxvcmllc1RvdGFsJywge1xuICAgIGdldDogZnVuY3Rpb24oKXtcbiAgICByZXR1cm4gIHRoaXMuY2Fsb3JpZXMgKiB0aGlzLndlaWdodCAvIDEwMFxuICAgIH1cbn0pXG5cblxuXG5Gb29kLm1lc3NhZ2UgPSAndGhpcyBpcyBpbiBmb29kIG1vZHVsZSdcblxubW9kdWxlLmV4cG9ydHMgPSBGb29kICIsImNvbnN0IEZvb2QgPSByZXF1aXJlKCcuL21vZGVscy9mb29kJylcbmNvbnN0IGJhY2tGb29kMmZvb2QgPSAoYmYpID0+ICh7XG4gICAgbmFtZTogYmYubmFtZSxcbiAgICBsaXBpZGVzOiBiZi5saXBpZHMsXG4gICAgZ2x1Y2lkZXM6IGJmLmNhcmJvaHlkcmF0ZXMsXG4gICAgcHJvdGVpbmVzOiBiZi5wcm90ZWlucyxcbiAgICB3ZWlnaHQ6IGJmLndlaWdodFxufSlcblxuY29uc3QgZm9vZDJiYWNrRm9vZCA9IChmKSA9PiAoe1xuICAgIG5hbWU6IGYubmFtZSxcbiAgICBsaXBpZHM6IGYubGlwaWRlcyxcbiAgICBjYXJib2h5ZHJhdGVzOiBmLmdsdWNpZGVzLFxuICAgIHByb3RlaW5zOiBmLnByb3RlaW5lcyxcbiAgICB3ZWl0aHQ6IGYud2VpZ2h0XG59KVxuXG5cbm1vZHVsZS5leHBvcnRzID0gKGJhY2tlbmQpID0+IHtcbiAgICBsZXQgc3RhdGUgPSBuZXcgUHJveHkoXG4gICAge1xuICAgICAgICBvYnNlcnZlcnM6IFtdLFxuICAgICAgICB2aWV3czogeycvJzogW119LFxuICAgICAgICB2aWV3OiAnLycsXG4gICAgICAgIGFjdGlvbnM6IHt9LFxuICAgICAgICBhZGRPYnNlcnZlcjogZnVuY3Rpb24obyl7dGhpcy5vYnNlcnZlcnMucHVzaChvKX1cbiAgICB9LFxuICAgIHtcbiAgICAgICAgc2V0OiBmdW5jdGlvbih0YXJnZXQsIHByb3AsIHZhbCl7XG4gICAgICAgIGNvbnN0IG9sZHZhbCA9IHRhcmdldFtwcm9wXVxuICAgICAgICB0YXJnZXRbcHJvcF0gPSB2YWxcbiAgICAgICAgc3dpdGNoIChwcm9wKSB7XG4gICAgICAgIGNhc2UgJ3ZpZXdzJzpcbiAgICAgICAgY2FzZSAnYWN0aW9ucyc6XG4gICAgICAgIGNhc2UgJ29ic2VydmVycyc6XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAndmlldyc6XG4gICAgICAgICAgICAvLyBoaWRlIG9sZCBjb21wb25lbnRzXG4gICAgICAgICAgICB0YXJnZXQudmlld3Nbb2xkdmFsXS5mb3JFYWNoKGMgPT4gYy5tb3VudFBvaW50LmhpZGRlbiA9IHRydWUpXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhgdmlldyBjaGFuZ2VkIHRvICR7dmFsfSwgd2lsbCBkaXNwbGF5IDpgKVxuICAgICAgICAgICAgY29uc29sZS5sb2codGFyZ2V0LnZpZXdzW3ZhbF0pXG4gICAgICAgICAgICB0YXJnZXQudmlld3NbdmFsXS5mb3JFYWNoKG8gPT4geyAvLyByZW5kZXIgdGhlbiBzaG93XG4gICAgICAgICAgICBvLnJlbmRlcihzdGF0ZSlcbiAgICAgICAgICAgIG8ubW91bnRQb2ludC5oaWRkZW4gPSBmYWxzZVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIGhpc3RvcnkucmVwbGFjZVN0YXRlKG51bGwsJycsIHZhbClcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdmFsXG4gICAgICAgIH0sXG4gICAgICAgIGdldDogZnVuY3Rpb24odGFyZ2V0LCBwcm9wKXtcbiAgICAgICAgICAgIHN3aXRjaCAocHJvcCkge1xuICAgICAgICAgICAgY2FzZSAndGFyZ2V0JzpcbiAgICAgICAgICAgIHJldHVybiB0YXJnZXRcbiAgICAgICAgICAgIGNhc2UgJ2JhY2tlbmQnOlxuICAgICAgICAgICAgcmV0dXJuIHRhcmdldC5iYWNrZW5kXG4gICAgICAgICAgICBjYXNlICdhY3Rpb25zJzpcbiAgICAgICAgICAgIHJldHVybiB0YXJnZXQuYWN0aW9uc1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIHJldHVybiB0YXJnZXRbcHJvcF1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB9XG4gICAgKVxuXG5zdGF0ZS50YXJnZXQuYmFja2VuZCA9IGJhY2tlbmRcblxuc3RhdGUuYWN0aW9ucy5sb2FkRm9vZCA9IGZ1bmN0aW9uKCl7XG4gICAgcmV0dXJuIGZldGNoKGAke3N0YXRlLmJhY2tlbmR9L2Zvb2RgKVxuICAgIC50aGVuKHJlcyA9PiByZXMuanNvbigpKVxuICAgIC50aGVuKGxpc3QgPT4gbGlzdC5tYXAocCA9PiBPYmplY3QuYXNzaWduKG5ldyBGb29kKGJhY2tGb29kMmZvb2QocCkpLCB7aWQ6IHAuaWR9KSkpXG4gICAgLnRoZW4ocGxhdHMgPT4gc3RhdGUucGxhdHMgPSBwbGF0cylcbn1cblxuc3RhdGUuYWN0aW9ucy5jcmVhdGVGb29kID0gKGZvb2QpID0+IHtcbiAgICBjb25zb2xlLmxvZyhgZm9vZCBjcmVhdGlvbiB3aXRoICR7Zm9vZH1gKVxuICAgIHJldHVybiBmZXRjaChgJHtzdGF0ZS5iYWNrZW5kfS9mb29kYCwge1xuICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgICAgICB9LFxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShmb29kKVxuICAgIH0pXG59XG5cbnN0YXRlLmFjdGlvbnMuZGVsZXRlRm9vZCA9ICh0YXJnZXRVcmwpID0+XG4gICAgZmV0Y2goYCR7c3RhdGUuYmFja2VuZH0ke3RhcmdldFVybH1gLFxuICAgIHtcbiAgICAgICAgbWV0aG9kOiAnZGVsZXRlJ1xuICAgIH1cbiAgICApXG4gICAgLnRoZW4oc3RhdGUuYWN0aW9ucy5sb2FkRm9vZClcblxuc3RhdGUuYWN0aW9ucy5lZGl0Rm9vZCA9IChlZGl0VXJsKSA9PiB7XG4gICAgY29uc29sZS5sb2coYGZvb2QgZWRpdGlvbiBwYWdlIDogJHtzdGF0ZS5iYWNrZW5kfSR7ZWRpdFVybH1gKVxuICAgIGNvbnNvbGUubG9nKGByZXNzb3VyY2Ugw6lkaXTDqWU6YCwgc3RhdGUucGxhdGUpXG4gICAgcmV0dXJuIGZldGNoKGAke3N0YXRlLmJhY2tlbmR9JHtlZGl0VXJsfWAsXG4gICAge1xuICAgICAgICBtZXRob2Q6ICdQVVQnLFxuICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAnQWNjZXNzLUNvbnRyb2wtQWxsb3ctSGVhZGVycyc6JyonLFxuICAgICAgICAgICAgJ0FjY2Vzcy1Db250cm9sLUFsbG93LU9yaWdpbic6JyonLFxuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgICAgICB9LFxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShzdGF0ZS5wbGF0ZSlcbiAgICB9KVxufVxuXG5yZXR1cm4gc3RhdGVcblxufSJdfQ==
