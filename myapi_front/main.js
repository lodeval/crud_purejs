// state
const State = require('./src/state')
const backend = 'http://localhost:2999'
let state = State(backend)
window.state = state

// controller
const controller = require('pubsub')
window.controller = controller

//compose UI: list of top elements
const listMountPoint = document.createElement('ul')
const counterMountPoint = document.createElement('div')
const addFoodMountPoint = document.createElement('button')
const createFormMountPoint = document.createElement('form')
const detailsMountPoint = document.createElement('div')
const editMountPoint = document.createElement('form')


// components
const listComp = require('./src/components/list')(controller.publish,listMountPoint)
const counterComp = require('./src/components/counter')(controller.publish, counterMountPoint)
const createComp = require('./src/components/addbutton')(controller.publish, addFoodMountPoint)
const createForm = require('./src/components/create_form')(controller.publish, createFormMountPoint)
const detailsComp = require('./src/components/detailed_list')(controller.publish, detailsMountPoint)
const editComp = require ('./src/components/edit') (controller.publish, editMountPoint)



const components = [ listComp, counterComp, createComp, createForm, detailsComp, editComp]
components.forEach(comp => state.addObserver(comp))
components.map(c => c.mountPoint).forEach(mp => document.body.appendChild(mp))

// views
state.views = {
    '/' : [listComp, counterComp, createComp],
    '/food': [listComp, counterComp, createComp],
    '/food/new': [createForm],
    'food/show': [detailsComp],
    '/food/edit': [editComp]
}

// controller configuration
controller.subscribe('addfood button clicked', () => {
    state.view = '/food/new'
    history.pushState("{}", "",state.view)
})
controller.subscribe(
    'init',
    () =>
    state.actions.loadFood()
    .then(() => state.view = '/food'),
    history.pushState("{}", "",'/food')
)
controller.subscribe(
    'create new ressource',
    (newFood) =>
    state.actions.createFood(newFood)
    .then(state.actions.loadFood)
    .then(() => state.view = '/food')
)
controller.subscribe(
    'update ressource',
    (newFood) =>
    state.actions.updatePlat(newFood)
        .then(state.actions.loadFood)
        .then(() => state.view = '/food')
)  
controller.subscribe(
    'edit resource',
    (url) => {
        state.actions.editFood(url)
        history.pushState("{}", "",'/food/edit')
        .then(() => state.view = '/food/edit')
    }
)

controller.subscribe(
    'edit plate', (url) => {
        state.actions.editFood(url)
        state.view='/food/edit',
        history.pushState("{}", "", state.view)
    }
)

controller.subscribe(
    'delete resource',
    (url) =>
    state.actions.deleteFood(url)
    .then(state.actions.loadFood)
    .then(() => state.view = '/food')
    .then(() => state.view = '/food')
)

controller.subscribe(
    'show',
    (url) => {
        let id = parseInt(url.replace('/food/',''))
        const obj = state.plats.find(o => o.id === id)
        state.element = obj
        state.views[url] = [detailsComp]
        state.view = url
    }
)

controller.subscribe(
    'edit button clicked',
    (url) => {
        let id = parseInt(url.replace('/food/',''))
        const obj = state.plats.find(o => o.id === id)
        state.element = obj
        state.views[url] = [editComp]
        state.view = url
    }
)

controller.subscribe(
    'edit element',
    (plat) =>
        state.actions.editFood(plat)
        .then(state.actions.loadFood)
        .then(() => state.view = '/food')
)

// application initialization

controller.publish('init') // initial action, fetch data, decide first view

window.addEventListener('popstate', (e) => {
    console.log('pathname = ${location.pathname}')
    state.view = location.pathname
})  